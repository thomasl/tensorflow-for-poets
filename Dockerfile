##
## Dockerfile for simple Tensorflow example
##  see also http://petewarden.com/2016/02/28/tensorflow-for-poets/
##
## Note that this file refers to various specific versions
## etc. so it will probably rot in a while.
##
## NOTE: Compilation takes a while (ten minutes or more
## on my laptop). Training likewise needs a while (about
## 20-30 minutes on my laptop for the 3700 images example).
##   There will be a lot of compiler warnings during build
## but no errors.
##
## See README.md for full installation.

FROM b.gcr.io/tensorflow/tensorflow:0.7.1-devel

ENV GITUSER  foo
ENV GITEMAIL foo@example.com
ENV DATA /tf_files

## Note: need to set the config for pull to succeed;
## the git repo is already present in the original image
##
## Note: we're checking out a longer commit with this prefix,
##  which ... um ... works at the moment. See the tutorial file
##  for the full commit id.

WORKDIR /tensorflow/
RUN git config --global user.name $GITUSER && \
    git config --global user.email $GITEMAIL && \
    git pull origin master
RUN git checkout 6d46c0b3

## Build some tools, this will take a while
## FIXME
## - seems like bazel only handles one tool at a time? very weird, we try
##   building two different images
##
## RUN bazel build -c opt --copt=-mavx tensorflow/examples/image_retraining:retrain
## RUN bazel build tensorflow/examples/label_image:label_image

VOLUME $DATA

## See Dockerfile.retrain and Dockerfile.label for images to use.
