
Dockerfiles for simple Tensorflow example
==========

 see also http://petewarden.com/2016/02/28/tensorflow-for-poets/

The Dockerfile builds the base image, Dockerfile.retrain and
Dockerfile.label then are used as shell scripts to run training
and image labeling.

STATUS: Preliminary; at the moment, you can read these files and adapt
them for your own use. The shell scripts show _examples_ of how to use
the images (according to the tutorial, in fact), but should be
generalized for more flexible use. Testing status: minimal.

NOTE: This repo refers to various specific images and versions
etc. so it will probably rot in a while. It follows the tutorial
above.

NOTE: Compilation takes a while (ten minutes or more per image on my
laptop). Training likewise needs a while (about 20-30 minutes from
scratch on my laptop for the 3700 images example).

NOTE: Due to what is apparently a quirk in the 'bazel' build
system, we can't use the same image for both retraining and labelling.
Thus, we build 'tfp.retrain' and 'tfp.label', which unfortunately
needs to compile everything twice. (Possibly fixable by someone
familiar with bazel.) See scripts 'retrain.sh'
and 'label.sh' for how to use images once built.

NOTE: The images get pretty big. The final tfp.label image is 3.7 GB on my
laptop for instance. (The initial tensorflow image pulled from google
is 2 GB.) Not very idiomatic docker, but I guess that's just how
Tensorflow works.

HOW TO INSTALL
----------

0. (Run VirtualBox etc and size your VM config to
   something powerful for your local machine; I set it 
   to 4 CPUs + 12 GB memory as
   suggested in the example.)

1. Create the volume to use and cd to that, for example:

     mkdir $(HOME)/git/tensorflow-data &&
     cd $(HOME)/git/tensorflow-data

2. Get and unpack the training images:

     curl -O http://download.tensorflow.org/example_images/flower_photos.tgz &&
     tar zxfv flower_photos.tgz

3. Build the images

     docker build -t tfp         -f Dockerfile .  && 
     docker build -t tfp.retrain -f Dockerfile.retrain .  && 
     docker build -t tfp.label   -f Dockerfile.label .

4. Run the image (train + label)

     sh retrain.sh &&
     sh label.sh

   or jump into the container directly if you want to explore

     docker run -it -v $(HOME)/git/tensorflow-data/:/tf_files tfp.retrain

Finally, for details see the retrain.sh and label.sh files for how to run the
images and specify paths, etc, and Dockerfile.retrain and Dockerfile.label
for the final build commands to be run. 

Tutorial by hand
----------

You can also run the google tensorflow image directly and build inside
that, like in the tutorial.

     docker run -it -v $(HOME)/git/tensorflow-data/:/tf_files b.gcr.io/tensorflow/tensorflow:0.7.1-devel)

Once you have completed the tutorial, use "docker commit" to save the
container state for later use. For example:

    $ docker ps
    ... locate container ID ...
    $ docker commit $CONTAINER_ID  tfp:1.0

Retraining and labeling
----------

Retraining and labeling is done by running the appropriate commands inside the container. See retrain.sh
and label.sh for examples on how to do this from the command line.

