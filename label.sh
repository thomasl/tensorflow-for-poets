#!/bin/bash
#
## Simplistic labeling according to tutorial.
##
## Requires that the trained network info is found in $VOL.
## (Basically, you have to run retrain.sh before this.)
##
## FIXME
## To make this more useful, get rid of all the fixed paths. For shame!
## The image is fixed too. Just awful.

DATA=/tf_files
VOL=$HOME/git/tensorflow-data
IMG=tfp.label
LABEL=\
"bazel-bin/tensorflow/examples/label_image/label_image \
  --graph=$DATA/retrained_graph.pb \
  --labels=$DATA/retrained_labels.txt \
  --output_layer=final_result \
  --image=$DATA/flower_photos/daisy/21652746_cc379e0eea_m.jpg"

docker run -it -v $VOL:$DATA $IMG /bin/bash -c $LABEL
