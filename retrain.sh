#!/bin/bash
#
## Simplistic retraining according to tutorial.
##
## Requires that the training data is found in $VOL, see below.
##
## FIXME
## To make this more useful, get rid of all the fixed paths. For shame!

DATA=/tf_files
VOL=$HOME/git/tensorflow-data
IMG=tfp.retrain
RETRAIN=\
"bazel-bin/tensorflow/examples/image_retraining/retrain \
       --bottleneck_dir=$DATA/bottlenecks \
       --model_dir=$DATA/inception \
       --output_graph=$DATA/retrained_graph.pb \
       --output_labels=$DATA/retrained_labels.txt \
       --image_dir=$DATA/flower_photos"
RETRAIN2="bazel-bin/tensorflow/examples/image_retraining/retrain  --bottleneck_dir=$DATA/bottlenecks  --model_dir=$DATA/inception  --output_graph=$DATA/retrained_graph.pb --output_labels=$DATA/retrained_labels.txt --image_dir=$DATA/flower_photos"

docker run -it --rm -v $VOL:$DATA $IMG /bin/sh -c $RETRAIN
# docker run -it -v $VOL:$DATA $IMG /bin/sh -c $RETRAIN2
# docker run -it -v $VOL:$DATA $IMG /bin/sh -c "bazel-bin/tensorflow/examples/image_retraining/retrain  --bottleneck_dir=$DATA/bottlenecks  --model_dir=$DATA/inception  --output_graph=$DATA/retrained_graph.pb --output_labels=$DATA/retrained_labels.txt --image_dir=$DATA/flower_photos"
